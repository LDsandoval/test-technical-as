## Instalación

Se requiere clonar el repositorio del siguiente enlace:

https://gitlab.com/LDsandoval/test-technical-as.git

O por medio de SSH

```bash
  git clone git@gitlab.com:LDsandoval/test-technical-as.git
```

Abrir la carpeta de archivos dentro de Visual Studio Code, para poder instalar dependencias requeridas.

```bash
  npm i
```

## Ejecución

Para ejecutar el proyecto ejecuté lo siguiente

```bash
  npm start
```

El proyecto funciona con una simulación de datos, por lo que no es necesaria una configuración extra.
