import { useState, useEffect } from "react";

const useFetch = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      try {
        if (!data) {
          const response = await fetch(url);
          const json = await response.json();
          setLoading(false);
          setData(json);
        }
      } catch (err) {
        setError(err);
      }
    };
    getData();
  }, [data, loading, url]);
  return { data, loading, error };
};

export default useFetch;
