import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addDataWeather } from "./features/weather/weatherSlice";
import { Pagination } from "antd";
import CardComponent from "./Components/CardComponent/CardComponent";
import useFetch from "./hooks/useFetch";

function App() {
  const { data, loading } = useFetch(
    "https://api.datos.gob.mx/v1/condiciones-atmosfericas"
  );
  const [paginationArray, setPAginationArray] = useState([]);
  const [pageSize] = useState(10);
  const [page, setPage] = useState(1);
  const conditionsState = useSelector((state) => state.weather);
  const dispatch = useDispatch();

  useEffect(() => {
    if (data) {
      dispatch(addDataWeather(data.results));
      const newArray = data.results.slice(
        (page - 1) * pageSize,
        page * pageSize
      );
      setPAginationArray(newArray);
    }
    setPage(parseInt(page));
  }, [data, dispatch, page, pageSize]);

  const handleChangePagination = (page) => {
    if (data) {
      const newArray = conditionsState.slice(
        (page - 1) * pageSize,
        page * pageSize
      );
      setPAginationArray(newArray);
      setPage(page);
    }
  };

  return (
    <div className='container-test'>
      {paginationArray.length > 0 &&
        loading &&
        paginationArray.map((item) => (
          <CardComponent
            key={item._id}
            id={item._id}
            citiid={item.cityid}
            name={item.name}
            state={item.state}
            probabilityofprecip={item.probabilityofprecip}
            relativehumidity={item.relativehumidity}
            Lastreporttime={item.Lastreporttime}
          />
        ))}
      <Pagination
        current={page}
        onChange={handleChangePagination}
        total={100}
      />
    </div>
  );
}

export default App;
