import React from "react";
import { render, screen } from "@testing-library/react";
import moment from "moment";
import ModalComponent from "./ModalComponent";

describe("ModalComponent", () => {
  const mockProps = {
    openModal: true,
    handleModal: jest.fn(),
    id: "123",
    citiid: "456",
    name: "Test City",
    state: "CA",
    probabilityofprecip: 80,
    relativehumidity: 60,
    Lastreporttime: moment().toISOString(),
  };

  it("renders the component with the correct props", () => {
    render(<ModalComponent {...mockProps} />);

    expect(screen.getByText("id")).toBeInTheDocument();
    expect(screen.getByText("123")).toBeInTheDocument();
    expect(screen.getByText("id city")).toBeInTheDocument();
    expect(screen.getByText("456")).toBeInTheDocument();
    expect(screen.getByText("name")).toBeInTheDocument();
    expect(screen.getByText("Test City")).toBeInTheDocument();
    expect(screen.getByText("Estado")).toBeInTheDocument();
    expect(screen.getByText("CA")).toBeInTheDocument();
    expect(screen.getByText("probabilityofprecip")).toBeInTheDocument();
    expect(screen.getByText("80 %")).toBeInTheDocument();
    expect(screen.getByText("relativehumidity:")).toBeInTheDocument();
    expect(screen.getByText("60 %")).toBeInTheDocument();
    expect(screen.getByText("probability:")).toBeInTheDocument();
    expect(screen.getByText("Si llueve")).toBeInTheDocument();
  });

  it("calls the handleModal function when the modal is closed", () => {
    render(<ModalComponent {...mockProps} />);

    const closeButton = screen.getByRole("button", { name: "Close" });
    closeButton.click();

    expect(mockProps.handleModal).toHaveBeenCalledTimes(1);
  });
});
