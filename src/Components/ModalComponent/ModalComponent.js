import React from "react";
import moment from "moment";
import { Modal } from "antd";

const ModalComponent = ({
  openModal,
  handleModal,
  id,
  citiid,
  name,
  state,
  probabilityofprecip,
  relativehumidity,
  Lastreporttime,
}) => {
  return (
    <Modal open={openModal} onOk={handleModal} onCancel={handleModal}>
      <div onClick={handleModal}>
        <div className='container-paragraph-modal'>
          <p className='paragraph-title'>id</p>
          <p className='paragraph-contain'>{id}</p>
        </div>
        <div className='container-paragraph-modal'>
          <p className='paragraph-title'>id city</p>
          <p className='paragraph-contain'>{citiid}</p>
        </div>
        <div className='container-paragraph-modal'>
          <p className='paragraph-title'>name</p>
          <p className='paragraph-contain'>{name}</p>
        </div>
        <div className='container-paragraph-modal'>
          <p className='paragraph-title'>Estado</p>
          <p className='paragraph-contain'>{state}</p>
        </div>
        <div className='container-paragraph-modal'>
          <p className='paragraph-title'>probabilityofprecip</p>
          <p className='paragraph-contain'>{probabilityofprecip} %</p>
        </div>
        <div className='container-paragraph-modal'>
          <p className='paragraph-title'>relativehumidity:</p>
          <p className='paragraph-contain'>{relativehumidity} %</p>
        </div>
        <div className='container-paragraph-modal'>
          <p className='paragraph-title'>Lastreporttime</p>
          <p className='paragraph-contain'>
            {moment(Lastreporttime).format("YYYY/MM/DD")}
          </p>
        </div>
        <div className='container-paragraph-modal'>
          <p className='paragraph-title'>probability:</p>
          <p className='paragraph-contain'>
            {probabilityofprecip > 60 || relativehumidity > 50
              ? "Si llueve"
              : "No llueve"}
          </p>
        </div>
      </div>
    </Modal>
  );
};

export default ModalComponent;
