import React from "react";
import { render } from "@testing-library/react";
import moment from "moment";
import CardComponent from "./CardComponent";

describe("CardComponent", () => {
  const mockProps = {
    id: "123",
    citiid: "456",
    name: "Test City",
    state: "Test State",
    probabilityofprecip: 80,
    relativehumidity: 60,
    Lastreporttime: moment().toISOString(),
  };

  test("renders component with correct props", () => {
    const { getByText } = render(<CardComponent {...mockProps} />);
    expect(getByText("id")).toBeInTheDocument();
    expect(getByText(mockProps.id)).toBeInTheDocument();
    expect(getByText("id city")).toBeInTheDocument();
    expect(getByText(mockProps.citiid)).toBeInTheDocument();
    expect(getByText("name")).toBeInTheDocument();
    expect(getByText(mockProps.name)).toBeInTheDocument();
    expect(getByText("Estado")).toBeInTheDocument();
    expect(getByText(mockProps.state)).toBeInTheDocument();
    expect(getByText("probabilityofprecip")).toBeInTheDocument();
    expect(getByText(`${mockProps.probabilityofprecip} %`)).toBeInTheDocument();
    expect(getByText("relativehumidity:")).toBeInTheDocument();
    expect(getByText(`${mockProps.relativehumidity} %`)).toBeInTheDocument();
    expect(getByText("Lastreporttime")).toBeInTheDocument();
    expect(
      getByText(moment(mockProps.Lastreporttime).format("YYYY/MM/DD"))
    ).toBeInTheDocument();
    expect(getByText("probability:")).toBeInTheDocument();
    expect(getByText("Si llueve")).toBeInTheDocument();
  });
});
