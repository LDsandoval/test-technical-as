import React, { useState } from "react";
import moment from "moment";
import ModalComponent from "../ModalComponent/ModalComponent";

const CardComponent = ({
  id,
  citiid,
  name,
  state,
  probabilityofprecip,
  relativehumidity,
  Lastreporttime,
}) => {
  const [isModal, setIsmodal] = useState(false);

  const handleModal = () => {
    setIsmodal(!isModal);
  };
  return (
    <>
      <ModalComponent
        openModal={isModal}
        handleModal={handleModal}
        id={id}
        citiid={citiid}
        name={name}
        state={state}
        probabilityofprecip={probabilityofprecip}
        relativehumidity={relativehumidity}
        Lastreporttime={Lastreporttime}
      />
      <div
        className='card-container'
        onClick={handleModal}
        data-testid='card-container'>
        <div className='container-paragraph'>
          <p className='paragraph-title'>id</p>
          <p className='paragraph-contain'>{id}</p>
        </div>
        <div className='container-paragraph'>
          <p className='paragraph-title'>id city</p>
          <p className='paragraph-contain'>{citiid}</p>
        </div>
        <div className='container-paragraph'>
          <p className='paragraph-title'>name</p>
          <p className='paragraph-contain'>{name}</p>
        </div>
        <div className='container-paragraph'>
          <p className='paragraph-title'>Estado</p>
          <p className='paragraph-contain'>{state}</p>
        </div>
        <div className='container-paragraph'>
          <p className='paragraph-title'>probabilityofprecip</p>
          <p className='paragraph-contain'>{probabilityofprecip} %</p>
        </div>
        <div className='container-paragraph'>
          <p className='paragraph-title'>relativehumidity:</p>
          <p className='paragraph-contain'>{relativehumidity} %</p>
        </div>
        <div className='container-paragraph'>
          <p className='paragraph-title'>Lastreporttime</p>
          <p className='paragraph-contain'>
            {moment(Lastreporttime).format("YYYY/MM/DD")}
          </p>
        </div>
        <div className='container-paragraph'>
          <p className='paragraph-title'>probability:</p>
          <p className='paragraph-contain'>
            {probabilityofprecip > 60 || relativehumidity > 50
              ? "Si llueve"
              : "No llueve"}
          </p>
        </div>
      </div>
    </>
  );
};

export default CardComponent;
