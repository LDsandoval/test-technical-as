import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

export const weatherSlice = createSlice({
  name: "weather",
  initialState,
  reducers: {
    addDataWeather: (state, action) => {
      if (state.length === 0) {
        state.push(...action.payload);
      }
    },
  },
});

export const { addDataWeather } = weatherSlice.actions;

export default weatherSlice.reducer;
